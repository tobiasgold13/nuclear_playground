package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

const maxDuration = 7
const timeout = 5
const numJobs = 10
const numWorkers = 3

// doHeavyAsShitWork takes between 1 - maxDuration s
func doHeavyAsShitWork(done chan<- bool) {
	sleepTime := time.Duration((rand.Intn(maxDuration) + 1)) * time.Second
	time.Sleep(sleepTime)
	done <- true
}

func log(txt ...interface{}) {
	now := time.Now()
	fmt.Printf("%v %v\n", now.Format("15:04:05.000000"), txt)
}

func worker(id int, jobs <-chan int, wg *sync.WaitGroup) {
	defer wg.Done()
	for j := range jobs {
		log("worker", id, "started job", j)
		done := make(chan bool)
		go doHeavyAsShitWork(done)
		select {
		case <-done:
			log("worker", id, "finished job", j)
		case <-time.After(timeout * time.Second):
			log("worker", id, "timed out job", j)
		}
	}
}

func main() {
	var wg sync.WaitGroup
	jobs := make(chan int, numJobs)

	for w := 1; w <= numWorkers; w++ {
		wg.Add(1)
		go worker(w, jobs, &wg)
	}

	for j := 1; j <= numJobs; j++ {
		jobs <- j
	}
	close(jobs)
	wg.Wait()
}

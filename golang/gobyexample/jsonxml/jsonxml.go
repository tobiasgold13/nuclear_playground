package jsonxml

import (
	"bytes"
	"encoding/json"
	"encoding/xml"
)

// Msg struct for (de)serialization
type Msg struct {
	Valid bool   `json:"valid" xml:"valid"`
	Text  string `json:"text" xml:"text"`
}

// Data struct for (de)serialization
type Data struct {
	ID   int    `json:"id" xml:"id,attr"`
	Name string `json:"name" xml:"name"`
	Msgs []Msg  `json:"msgs" xml:"msgs>msg"`
}

// UnmarshalFromJSONString deserializes a Data struct from string data (panic if error)
func UnmarshalFromJSONString(jsonString string) *Data {
	unmarshalledData := &Data{}
	if err := json.Unmarshal([]byte(jsonString), unmarshalledData); err != nil {
		panic(err)
	}
	return unmarshalledData
}

// MarshalToXMLString returns xml string
func MarshalToXMLString(data *Data) string {
	xmlData, err := xml.MarshalIndent(data, " ", " ")
	// xmlData, err := xml.Marshal(data)
	if err != nil {
		panic(err)
	}
	return string(xmlData)
}

// UnmarshalFromXMLString deserializes a Data struct from string data (panic if error)
func UnmarshalFromXMLString(xmlString string) *Data {
	unmarshalledData := &Data{}
	if err := xml.Unmarshal([]byte(xmlString), &unmarshalledData); err != nil {
		panic(err)
	}
	return unmarshalledData
}

// MarshalToJSONString returns json string
func MarshalToJSONString(data *Data) string {
	jsonData, err := json.Marshal(data)
	// jsonData, err := json.MarshalIndent(data, " ", " ")
	if err != nil {
		panic(err)
	}
	return string(jsonData)
}

// CompactJSONString compacts a json string
func CompactJSONString(str string) string {
	compactedBuffer := bytes.Buffer{}
	if err := json.Compact(&compactedBuffer, []byte(str)); err != nil {
		panic(err)
	}
	return string(compactedBuffer.Bytes())
}

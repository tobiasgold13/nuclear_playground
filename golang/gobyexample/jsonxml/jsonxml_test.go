package jsonxml

import (
	"testing"
)

const initialJSONString = `
	{
		"id": 5,
		"name": "golang",
		"msgs": [
			{
				"valid": true,
				"text": "I like brownies ;)"
			},
			{
				"valid": false,
				"text": "Drugs are bad."
			}
		],
		"unknown": "property"
	}
`

func TestUnmarshalFromJSON(t *testing.T) {
	data := UnmarshalFromJSONString(initialJSONString)
	if data.ID != 5 {
		t.Errorf("Error: invalid id")
	}
	if len(data.Msgs) != 2 {
		t.Errorf("Error: invalid messages")
	}
}

func TestEverything(t *testing.T) {
	unmarshalFromJSONString := UnmarshalFromJSONString(initialJSONString)
	xmlString := MarshalToXMLString(unmarshalFromJSONString)
	unmarshalFromXMLString := UnmarshalFromXMLString(xmlString)
	jsonString := MarshalToJSONString(unmarshalFromXMLString)

	// remove the unused property from the compacted version
	expected := CompactJSONString(initialJSONString)
	expected = expected[:115] + expected[136:]
	actual := CompactJSONString(jsonString)
	if expected != actual {
		t.Errorf("Error: resulting json strings unequal")
		t.Errorf("expected: %v", expected)
		t.Errorf("but was:  %v", actual)
	}
}

package main

import (
	"fmt"
	"net/http"

	h "gitlab.com/tobiasgold13/nuclear_playground/golang/gobyexapmle/http/handlers"
)

func handle(path string, handler h.AppHandler) {
	http.Handle(path, h.AppHandler(handler))
}

func main() {
	handle("/", h.Hello)
	handle("/headers", h.Headers)
	handle("/dangerous", h.ErrorProne)
	fmt.Println("server running at http://localhost:8090/")
	http.ListenAndServe(":8090", nil)
}

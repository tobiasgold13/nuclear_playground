package handlers

import (
	"fmt"
	"net/http"
)

// Headers handler
func Headers(w http.ResponseWriter, req *http.Request) error {
	for name, headers := range req.Header {
		for _, h := range headers {
			fmt.Fprintf(w, "%v: %v\n", name, h)
		}
	}
	return nil
}

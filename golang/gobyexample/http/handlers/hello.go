package handlers

import (
	"fmt"
	"net/http"
	"time"
)

// Hello handler
func Hello(w http.ResponseWriter, req *http.Request) error {
	ctx := req.Context()
	select {
	case <-time.After(5 * time.Second):
		fmt.Fprintln(w, "hello")
	case <-ctx.Done():
		err := ctx.Err()
		fmt.Println("error in /hello:", err)
		return err
	}
	return nil
}

package handlers

import (
	"errors"
	"fmt"
	"math/rand"
	"net/http"
)

// ErrorProne some really error prone stuff happens here...
func ErrorProne(w http.ResponseWriter, r *http.Request) error {
	if r := rand.Intn(10); r < 5 {
		fmt.Fprintln(w, "successful")
		return nil
	}
	err := errors.New("something's clearly wrong")
	return err
}

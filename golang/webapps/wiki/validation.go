package wiki

import (
	"errors"
	"net/http"
	"regexp"
)

var validPath = regexp.MustCompile("^/(edit|save|view)/([a-zA-Z0-9]+)$")

// GetTitle extracts the title from the request
func getTitle(r *http.Request) (string, error) {
	match := validPath.FindStringSubmatch(r.URL.Path)
	if match == nil {
		return "", errors.New("invalid page title")
	}
	return match[2], nil
}

// MakeHandler decorates a HandlerFunc with the title
func MakeHandler(fn func(http.ResponseWriter, *http.Request, string)) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		title, err := getTitle(r)
		if err != nil {
			http.Error(w, err.Error(), http.StatusNotFound)
			return
		}
		fn(w, r, title)
	}
}

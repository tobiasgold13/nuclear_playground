package wiki

import (
	"fmt"
	"html/template"
	"io/ioutil"
	"os"
	"regexp"
	"strings"
)

// TODO unit tests & coverage?

// Page stores all relevant data
type Page struct {
	Title string
	Data  []byte
	Body  template.HTML
}

const dataDir = "data/"

func getFilename(title string) string {
	return dataDir + strings.ToLower(title) + ".txt"
}

// Save stores the page in p.Title.txt
func (p *Page) Save() error {
	if _, err := os.Stat(dataDir); os.IsNotExist(err) {
		os.Mkdir(dataDir, os.ModePerm)
	}
	filename := getFilename(p.Title)
	return ioutil.WriteFile(filename, p.Data, 0600)
}

var pageLinkRegex = regexp.MustCompile("\\[([a-zA-Z0-9])+\\]")

func generateBody(data []byte) template.HTML {
	body := pageLinkRegex.ReplaceAllFunc(data, func(match []byte) []byte {
		pageTitle := string(match)[1 : len(match)-1]
		pageLink := fmt.Sprintf(`<a href="/view/%v">%v</a>`, pageTitle, pageTitle)
		return []byte(pageLink)
	})
	return template.HTML(body)
}

// LoadPage loads a page from a file with the specific name
func LoadPage(title string) (*Page, error) {
	filename := getFilename(title)
	body, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	return &Page{Title: title, Data: body, Body: generateBody(body)}, nil
}

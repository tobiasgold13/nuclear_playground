package main

import (
	wiki "gitlab.com/tobiasgold13/nuclear_playground/golang/webapps/wiki"
)

func main() {
	wiki.Start("8090")
}

# Go

https://dev.to/napolux/how-to-write-a-microservice-in-go-with-go-kit-a66

```shell
$ go run main.go
```

OR

```shell
$ go build main.go
$ ./main
```
package napodate

import (
	"context"
	"testing"
	"time"
)

func setup() (src Service, ctx context.Context) {
	return NewService(), context.Background()
}

func TestStatus(t *testing.T) {
	srv, ctx := setup()

	s, err := srv.Status(ctx)
	if err != nil {
		t.Errorf("Error: %s", err)
	}

	if s != "ok" {
		t.Errorf("expected service to be ok")
	}
}

func TestGet(t *testing.T) {
	srv, ctx := setup()
	d, err := srv.Get(ctx)
	if err != nil {
		t.Errorf("Error: %s", err)
	}

	time := time.Now()
	today := time.Format("02/01/2006")

	if today != d {
		t.Errorf("expected dates to be equal")
	}
}

func TestValidate(t *testing.T) {
	srv, ctx := setup()
	b, err := srv.Validate(ctx, "31/12/2019")
	if err != nil {
		t.Errorf("Error: %s", err)
	}

	if !b {
		t.Errorf("date should be valid")
	}

	b, err = srv.Validate(ctx, "31/31/2019")
	if b {
		t.Errorf("invalid date should be invalid")
	}
}

module gitlab.com/tobiasgold13/nuclear_playground/golang/gokit

go 1.15

require (
	github.com/go-kit/kit v0.10.0
	github.com/gorilla/mux v1.8.0
)

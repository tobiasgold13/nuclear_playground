# css-stuff

fooling around with any sort of css stuff, because I have no idea what I'm doing and what can be done.

* started with fancy css animation stuff (easing and whatever)
* some layout/structuring stuff up next (flex box, grid)

PS: my styling always looks like shit. doing this just for fun.

## http
at the moment, this project just contains static html files with inline css. it's just trying out stuff ;)
If you want to try it on your phone, you can serve it with the following command. This will serve the current dir via http. Instructions will be printed on the screen.

```shell
$ npx serve
```

> You need an up-to-date version of *npm* installed to have the *npx* command. Guess it's a nice solution for serving stuff over http for dev purposes and not creating a huge project around.

## references
* [Basic HTML structure](https://css-tricks.com/snippets/html/html5-page-structure/) for copy and paste purposes.
* [Random animation tutorial](https://levelup.gitconnected.com/css-and-javascript-animation-tips-with-an-example-in-angular-678246901752)

## look at
* [predefined animations](https://daneden.github.io/animate.css/)
* [flex](https://css-tricks.com/snippets/css/a-guide-to-flexbox/) and [grid](https://css-tricks.com/snippets/css/complete-guide-grid/) guides
* [grid by example](https://gridbyexample.com/examples/)
# dizzy-tags

_Tag files of any kind and manage them in folders_

* create file collections (represent folder structure)
* abstract folder structure from filesystem (eg S3 for storage)
* upload file or download from url
* preview (first step just images)
* react with server side rendering
* test-driven
* export folders as archive
* tag every file with n tags
* show all images with tags
* search for tags
* export all images with tag as archive
* auto find duplicates on upload (or with cronjob), based on content - not image size
* upload multiple files with batch "tagging"

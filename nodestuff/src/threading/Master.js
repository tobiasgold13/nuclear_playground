(function() {
    'use strict';
  
    const path = require('path');
    const {fork} = require('child_process');
    const numCPUs = require('os').cpus().length;
  
    (function launch() {
    //   global._test = "hier wird was in die globals im master geschrieben";
      console.log('launchy...');
      for (let i = 0; i < numCPUs; ++i) {
        let child = fork(path.join(__dirname, 'Child.js'));
        child.on('message', msg => {
          console.log("message arrived from child", i, ":", msg);
          if (!(i%2)) child.kill();
        });
        setTimeout(() => {
          child.send({
            msg: "message from master to child " + i,
            childId: i
          });
        }, 1000);
      }
    })();
  
  })();
  
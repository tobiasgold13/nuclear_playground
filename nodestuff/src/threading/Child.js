(function() {
    'use strict';
  
    console.log("starting child....");
  
    let id = -1;
  
    process.on('message', msg => {
      console.log("received message in child: ", msg.msg);
      id = msg.childId;
  
      setTimeout(() => {
        // nothing is shared between master and children
        // console.log("global in child:", _test);
        process.send("this is a message from a child " + id);
        console.log("finished child", id, "!");
        if (id % 2) process.exit(0);
      }, id * 1000);
    });
  
  
  })();
  
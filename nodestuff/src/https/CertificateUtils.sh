#!/bin/bash
# https://medium.com/@sevcsik/authentication-using-https-client-certificates-3c9d270e8326

CERT_DIR='./ssl'

CMD_COLOR='\033[0;37m'
IN_COLOR='\033[0;31m'
NO_COLOR='\033[0m'

function usage() {
    echo -e "create \t- create all dummy self-signed certificates to $CERT_DIR"
    echo -e "test \t- test certificate types"
    echo -e "clean \t- rm cert dir ($CERT_DIR)"
}

function createCerts() {
    if [ ! -d $CERT_DIR ]; then
        mkdir $CERT_DIR
    fi

    echo -e "${CMD_COLOR}generating server key (server_key.pem) and cert (server_cert.pem)${NO_COLOR}"
    openssl req -x509 -newkey rsa:4096 -keyout $CERT_DIR/server_key.pem -out $CERT_DIR/server_cert.pem -nodes -days 365 -subj "/CN=localhost/O=Certificate Demo"

    echo -e "${CMD_COLOR}generating client key (alice_key.pem) and client cert (alice_csr.pem) that will be signed with server cert${NO_COLOR}"
    openssl req -newkey rsa:4096 -keyout $CERT_DIR/alice_key.pem -out $CERT_DIR/alice_csr.pem -nodes -days 365 -subj "/CN=Alice"
    echo -e "${CMD_COLOR}sign client cert (alice_csr.pem) with server cert (server_cert.pem) and save to cert file (alice_cert.pem)${NO_COLOR}"
    openssl x509 -req -in $CERT_DIR/alice_csr.pem -CA $CERT_DIR/server_cert.pem -CAkey $CERT_DIR/server_key.pem -out $CERT_DIR/alice_cert.pem -set_serial 01 -days 365

    echo -e "${CMD_COLOR}generate non signed client key (bob_key.pem) and cert (bob_csr.pem) and self sign it to (bob_cert.pem)${NO_COLOR}"
    openssl req -newkey rsa:4096 -keyout $CERT_DIR/bob_key.pem -out $CERT_DIR/bob_csr.pem -nodes -days 365 -subj "/CN=Bob"
    openssl x509 -req -in $CERT_DIR/bob_csr.pem -signkey $CERT_DIR/bob_key.pem -out $CERT_DIR/bob_cert.pem -days 365

    echo -e "${CMD_COLOR}generate browser bundle PKCS#12 for both client certs (alice.p12, bob.p12)${NO_COLOR}"
    echo -ne "${IN_COLOR}enter password used for both certs: ${NO_COLOR}"
    read -s PASSWORD
    echo ""
    openssl pkcs12 -export -clcerts -in $CERT_DIR/alice_cert.pem -inkey $CERT_DIR/alice_key.pem -out $CERT_DIR/alice.p12 -password pass:$PASSWORD
    openssl pkcs12 -export -clcerts -in $CERT_DIR/bob_cert.pem -inkey $CERT_DIR/bob_key.pem -out $CERT_DIR/bob.p12 -password pass:$PASSWORD
}

function test() {
    echo -e "${CMD_COLOR}testing root url${NO_COLOR}"
    curl --insecure https://localhost:1337/

    echo -e "${CMD_COLOR}testing /authenticate without certs${NO_COLOR}"
    curl --insecure https://localhost:1337/authenticate
    
    echo -ne "${IN_COLOR}enter password used for client certs: ${NO_COLOR}"
    read -s PASSWORD
    echo ""

    echo -e "${CMD_COLOR}testing /authenticate with signed cert${NO_COLOR}"
    curl --insecure --cert $CERT_DIR/alice.p12:$PASSWORD --cert-type p12 https://localhost:1337/authenticate
    
    echo -e "${CMD_COLOR}testing /authenticate with unsigned cert${NO_COLOR}"
    curl --insecure --cert $CERT_DIR/bob.p12:$PASSWORD --cert-type p12 https://localhost:1337/authenticate
}

case $1 in
    create)
        createCerts
    ;;

    test)
        test
    ;;

    clean)
        rm -r $CERT_DIR
    ;;

    *)
        usage
    ;;
esac

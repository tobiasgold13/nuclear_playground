(function () {
    'use strict';

    // https://medium.com/@sevcsik/authentication-using-https-client-certificates-3c9d270e8326

    const fs = require('fs');
    const path = require('path');
    const https = require('https');
    const express = require('express');

    const port = 1337;

    let options = {
        key: fs.readFileSync(path.join(__dirname, 'ssl/server_key.pem')),
        cert: fs.readFileSync(path.join(__dirname, 'ssl/server_cert.pem')),
        requestCert: true,
        rejectUnauthorized: false,
        ca: [fs.readFileSync(path.join(__dirname, 'ssl/server_cert.pem'))]
    };

    let app = express();

    app.get('/', (req, res) => {
        res.send('<a href="authenticate">Log in using client certificate</a>\n')
    });

    app.get('/authenticate', (req, res) => {
        const cert = req.connection.getPeerCertificate()
        if (req.client.authorized) {
            res.send(`Hello ${cert.subject.CN}, your certificate was issued by ${cert.issuer.CN}!\n`)
        } else if (cert.subject) {
            res.status(403)
                .send(`Sorry ${cert.subject.CN}, certificates from ${cert.issuer.CN} are not welcome here.\n`)
        } else {
            res.status(401)
                .send(`Sorry, but you need to provide a client certificate to continue.\n`)
        }
    });

    https.createServer(options, app).listen(port, () => { console.log(`server listening on port ${port}`); });

})();
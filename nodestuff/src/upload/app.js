(function() {
  'use strict';

  const express = require('express');
  const app = express();
  const path = require('path');
  const formidable = require('formidable');
  const fs = require('fs');

  console.log("public", path.join(__dirname, '/public'));

  app.use('/', express.static(path.join(__dirname, 'public')));

  app.get('/upload', (req, res) => {
    res.sendFile(path.join(__dirname, 'views/upload.html'));
  });

  app.post('/upload', (req, res) => {
    // create an incoming form object
    let form = new formidable.IncomingForm();
    form.multiples = true;
    form.uploadDir = path.join(__dirname, '/public');

    // every time a file has been uploaded successfully, rename it to it's orignal name
    form.on('file', (field, file) => {
      fs.rename(file.path, path.join(form.uploadDir, file.name), (err) => {
        if (err) return console.log(err);
      });
    });

    // log any errors that occur
    form.on('error', (err) => {
      console.log('An error has occured: \n' + err);
    });

    // once all the files have been uploaded, send a response to the client
    form.on('end', () => {
      res.end('success');
    });

    // parse the incoming request containing the form data
    form.parse(req);
  });

  let server = app.listen(3000, () => {
    console.log('Server listening on port 3000');
  });

})();

# playground

Just a mono repo with various projects for testing stuff, tutorials, trying out some tech shit etc.
I was just sick of creating a new repo for every tutorial etc. so a I created this mono repo. ~~This will be mainly node/typescript/react projects and similar technologies.~~

*In some projects, the link to the tutorial is in the description field of package.json.*

| folder                                       | notes                                                                               |
|----------------------------------------------|-------------------------------------------------------------------------------------|
| [nodestuff](./nodestuff/)                    | • NodeJS Stuff</br> • https</br> • threading (master/child)</br> • file upload</br> |
| [tsdecorators](./tsdecorators/)              | • testing decorator functions with typescript                                       |
| [css-stuff](./css-stuff/)                    | • some css stuff for somebody that has no frontend experience                       |
| [tic-tac-vue](./tic-tac-vue/)                | • build a tic tac toe with vue.js                                                   |
| [golang](./golang/)                          | • trying/learning go                                                                |

# tic-tac-vue

> The first time I touched vue.js with the help of [this](https://levelup.gitconnected.com/building-tic-tac-toe-in-vue-js-57a23822313d) tutorial.

- [Interesting stuff](https://vuejsdevelopers.com/2017/03/05/vue-js-reactivity/) about vue.js reactivity.

## Build Setup

```bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```

For detailed explanation on how things work, consult the [docs for vue-loader](http://vuejs.github.io/vue-loader).

## TODO

For the sake of learning, this should have been done test driven or tests should be added incl. coverage.

Somehow I think this tutorial is quite shitty. It's not simplifying stuff, it's teaching anti-patterns here and there. I still just copied it, because I never touched vue and I'm tired and drinking. So what can go wrong, if I try to improve stuff without tests and without a working example? ;)
Hope I'll edit and fix all this in the future.

### PS

Somehow there is a hard linter & formatter running on that stuff, but I have no idea where it comes from. Can't remember I configured anything....

export function ClassDecorator(constructor: any) {
    console.log(`Decorating ${constructor.name}`);
}

export function MethodDecorator(
    target: any,
    propertyKey: string | symbol,
    descriptor: PropertyDescriptor,
) {
    console.log(
        `Decorating method ${String(propertyKey)}` +
        ` from ${target.constructor.name}`,
    );
}

export function ParameterDecorator(
    target: any,
    propertyKey: string | symbol,
    parameterIndex: number,
) {
    console.log(
        `Decorating parameter ${String(propertyKey)}` +
        ` (index ${parameterIndex})` +
        ` from ${target.constructor.name}`,
    );
}

export function PropertyDecorator(
    target: any,
    propertyKey: string | symbol,
) {
    console.log(
        `Decorating property ${String(propertyKey)}` +
        ` from ${target.constructor.name}`,
    );
}
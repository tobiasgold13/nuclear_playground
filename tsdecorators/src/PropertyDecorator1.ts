function reflect() {
    return function(target: Test, propertyKey: string | symbol) {
        console.log(`Decorating property "${propertyKey.toString()}" from object "${target.constructor.name}"`)
    }
}

function reflect2(target: Test, propertyKey: string | symbol) {
    console.log(`Decorating property "${propertyKey.toString()}" from object "${target.constructor.name}"`)
}

export class Test {
    @reflect()
    @reflect2
    private name: string = "initial value"

    constructor(name: string) {
        this.name = name
    }

    public getName() {
        return this.name
    }
}

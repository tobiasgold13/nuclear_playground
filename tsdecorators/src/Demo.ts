import { ClassDecorator, PropertyDecorator, MethodDecorator, ParameterDecorator } from './Decorators'

@ClassDecorator
export class Demo {
    @PropertyDecorator
    public foo: string = "value of foo"

    constructor() {
        console.log("Simple class initialized")
        this.writeGreeting()
    }

    @MethodDecorator
    public bar() {
        return "return value of bar"
    }

    @MethodDecorator
    public writeGreeting(@ParameterDecorator greeting: string = "Hello, world") {
        console.log(greeting)
    }
}

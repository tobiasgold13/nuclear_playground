// https://gist.github.com/remojansen/16c661a7afd68e22ac6e#file-property_decorator-ts

function logProperty(target: any, key: string) {

    // property value
    var _val = target[key];

    // property getter
    var getter = function () {
        console.log(`Get: ${key} => ${_val}`);
        return _val;
    };

    // property setter
    var setter = function (newVal: any) {
        console.log(`Set: ${key} => ${newVal}`);
        _val = newVal;
    };

    // Delete property.
    if (delete target[key]) {

        // Create new property with getter and setter
        Object.defineProperty(target, key, {
            get: getter,
            set: setter,
            enumerable: true,
            configurable: true
        });
    }
}

class Person {
    @logProperty
    public name: string;
    public surname: string;

    constructor(name: string, surname: string) {
        this.name = name;
        this.surname = surname;
    }
}

var p = new Person("remo", "Jansen")
p.name = "Remo"
var n = p.name
p.name = "new name"

var p2 = new Person("nobody", "atall")
// p2.name = "styg"
console.log("p1 name: %s", p.name)
console.log("p2 name: %s", p2.name)
import { expect } from 'chai'
import { Demo } from './Demo'

describe('Demo', () => {

    it('testing class demo', () => {
        let d = new Demo()
        expect(d.bar()).to.be.equal("return value of bar")
    })

})
import { expect } from 'chai'
import { Test } from './PropertyDecorator1'

describe('PropertyDecorator1', () => {

    it('testing class Test', () => {
        let t = new Test("some name")
        expect(t.getName()).to.be.equal("some name")
    })

})